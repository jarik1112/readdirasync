import { Dirent } from 'fs';
import { readdir as rd } from 'fs/promises';
import { join } from 'path';

export type ReaddirResult = (Dirent | string)[];

export type ReaddirOptions = Parameters<typeof rd>[1];

const dirsOnly = (list: Dirent[]) => list.filter((i) => i.isDirectory());
const notDirs = (list: Dirent[], cb: (val: Dirent) => Dirent | string) =>
  list.filter((i) => !i.isDirectory()).map(cb);

export function readdirRecursive(path: string, options?: ReaddirOptions): Promise<ReaddirResult> {
  const pathJoin = (i: Dirent) => join(path, i.name);
  const resFn = !options?.withFileTypes ? (i: Dirent) => pathJoin(i) : (i: Dirent) => i;

  return rd(path, { withFileTypes: true }).then((result) =>
    dirsOnly(result).reduce(
      (res: Promise<ReaddirResult>, c: Dirent): Promise<ReaddirResult> =>
        res.then(
          (r: ReaddirResult): Promise<ReaddirResult> =>
            readdirRecursive(pathJoin(c), options).then(
              (subRes): ReaddirResult => [...r, ...subRes],
            ),
        ),
      Promise.resolve([...notDirs(result, resFn)]),
    ),
  );
}
