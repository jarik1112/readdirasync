/// <reference types="node" />
import { Dirent } from 'fs';
import { readdir as rd } from 'fs/promises';
export declare type ReaddirResult = (Dirent | string)[];
export declare type ReaddirOptions = Parameters<typeof rd>[1];
export declare function readdirRecursive(path: string, options?: ReaddirOptions): Promise<ReaddirResult>;
