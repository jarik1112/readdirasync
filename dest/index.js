"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.readdirRecursive = void 0;
const promises_1 = require("fs/promises");
const path_1 = require("path");
const dirsOnly = (list) => list.filter((i) => i.isDirectory());
const notDirs = (list, cb) => list.filter((i) => !i.isDirectory()).map(cb);
function readdirRecursive(path, options) {
    const pathJoin = (i) => path_1.join(path, i.name);
    const resFn = !(options === null || options === void 0 ? void 0 : options.withFileTypes) ? (i) => pathJoin(i) : (i) => i;
    return promises_1.readdir(path, { withFileTypes: true }).then((result) => dirsOnly(result).reduce((res, c) => res.then((r) => readdirRecursive(pathJoin(c), options).then((subRes) => [...r, ...subRes])), Promise.resolve([...notDirs(result, resFn)])));
}
exports.readdirRecursive = readdirRecursive;
//# sourceMappingURL=index.js.map