"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const index_1 = require("./index");
const path_1 = require("path");
const test = async () => {
    let res = [];
    console.time('test');
    try {
        res = await index_1.readdirRecursive(path_1.resolve('../shadowfox/packages'), { withFileTypes: true });
    }
    catch (e) {
        console.error(e);
    }
    console.timeEnd('test');
    console.log(res.length);
};
(async () => {
    await test();
    await test();
    await test();
    await test();
    await test();
})();
//# sourceMappingURL=test.js.map